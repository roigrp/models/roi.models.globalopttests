R ?= R
pkg=ROI.models.globalOptTests

docs:
	$(R) -e "Sys.setenv(ROI_LOAD_PLUGINS = FALSE); library(roxygen2); roxygenise(roclets=c('rd'))"

build:
	$(R) CMD build .

inst: build
	$(R) CMD INSTALL $(pkg)*.tar.gz
	
check: build
	$(R) CMD check $(pkg)*.tar.gz

check_as_cran: build
	$(R) CMD check --as-cran $(pkg)*.tar.gz

devcheck_win_devel: build
	$(R) -e "devtools::check_win_devel(email = 'FlorianSchwendinger@gmx.at')"
